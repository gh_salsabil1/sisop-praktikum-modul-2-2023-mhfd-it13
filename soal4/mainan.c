#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>



int main(int argc, char *argv[]){
  char *cron_hours, *cron_minutes, *cron_seconds;

  cron_hours = argv[1];
  if (strcmp(cron_hours, "*") == 0) {
    cron_hours = "0";
  }

  cron_minutes = argv[2];
  if (strcmp(cron_minutes, "*") == 0) {
    cron_minutes = "0";
  }

  cron_seconds = argv[3];
  if (strcmp(cron_seconds, "*") == 0) {
    cron_seconds = "0";
  }

  int hours = atoi(cron_hours); 
  int minutes = atoi(cron_minutes); 
  int seconds = atoi(cron_seconds);

  if(hours > 23 || minutes > 59 || seconds > 59){
    printf("Invalid input value(s) for minutes, hours, or seconds\n");
    return 1;
  }

  if(access(argv[4], X_OK) == -1 ){
      fprintf(stderr, "File not found: %s\n", argv[4]);
      return 1;
    }

  time_t now = time(NULL);
  struct tm *tm_now = localtime(&now);

  struct tm tm_cron = *tm_now;
  tm_cron.tm_hour = hours;
  tm_cron.tm_min = minutes;
  tm_cron.tm_sec = seconds;

  pid_t pid = fork();

  if(pid == 0){
      while(1){

      time_t next_time = mktime(&tm_cron);

      if(next_time < now){
        next_time += 86400;
      }

      int sleep_time = (int)difftime(next_time,now);
      
      sleep(sleep_time);
      
      char *script_path = argv[4]; 
      char *args[] = {"sh", script_path, NULL};

      pid_t child_process = fork();

      if(child_process == 0){
        int result = execvp("sh", args);
        if(result == -1){
        perror("execvp");
        return EXIT_FAILURE; 
        }
      }
    }
  } 
  return 0;
}
