# sisop-praktikum-modul-2-2023-mh-it13
## daftar isi ##
- [anggota kelompok](#anggota-kelompok)
- [nomor 1](#nomor-1)
    - [soal  1.a](#1a)
    - [soal  1.b](#1b)
    - [soal  1.c](#1c)
    - [soal  1.d](#1d)
- [nomor 2](#nomor-2)
    - [soal  2.1](#21)
    - [soal  2.2](#22)
    - [soal  2.3](#23)
    - [soal  2.4](#24)
- [nomor 3](#nomor-3)
    - [soal  3.a](#3a)
    - [soal  3.b](#3b)
    - [hasil](#3hasil)
- [nomor 4](#nomor-4)
    - [hasil](#4hasil)
- [kendala](#kendala)


## anggota kelompok

| nrp        | nama                       |
| ---------- | -------------------------- |
| 5027211038 | ahnaf musyaffa             |
| 5027211051 | wisnu adjie saka           |
| 5027211062 | anisa ghina salsabila      |

## nomor 1

### 1.hasil


## nomor 2

1. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

2. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

3. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

4. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

5. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan :
1. Tidak boleh menggunakan system()
2. Proses berjalan secara daemon
3. Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

### 2.1
```C
void createFolder(){
  time_t t;
  time(&t);
  struct tm *local = localtime(&t);
  char timeFormat[50], URL[50] = "https://picsum.photos/", str[50], fileFormat[50];
  strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);
  char *argv[] = {"mkdir", "-p", timeFormat, NULL};
  if(fork() == 0){
      downloadImage(timeFormat);
  } 
  else{ 
    execv("/bin/mkdir", argv);
  }
}
```
Pada fungsi createFolder memiliki tugas untuk membuat folder yang akan dilaksanakan secara daemon 30 detik sekali.
`strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);` berfungsi untuk mengubah local format menjadi format yang diinginkan. Kemudian terdapat argv yang merupakan argumen yang akan dieksekusi di parent id dengan command `execv("/bin/mkdir", argv);`. Pada child process akan dilakukan downloadImage 

### 2.2
```C
void downloadImage(char* folder_name){
    strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);
    sprintf(fileFormat, "./%s/%s.jpg", folder_name, timeFormat); 

    current_time = time(NULL);
    current_time = (current_time % 1000) + 50;  
    sprintf(str, "%ld", current_time);

    strcat(URL, str);
    
    char *argv[] = {"wget", "-O", fileFormat, URL , NULL}; 

    pid = fork();

    if(pid == 0){
        execvp("wget", argv);
    }

    sleep(5);
}
```
Potongan kode diatas bertujuan untuk melakukan download image. 
`sprintf(fileFormat, "./%s/%s.jpg", folder_name, timeFormat);` kode tersebut untuk mendefine path dari file yang akan di download. Dengan folder name adalah argumen yang diterima dari createFolder dan file name adalah waktu sekarang. Kemudian terdapat argv yang berisi command yang selanjutnya akan dieksekusi pada child process dengan command `execvp("wget", argv)`

### 2.3
```C
void removeDir(char* folder_name){
  char *argv[] = {"rm", "-rf", folder_name, NULL};
  execv("/bin/rm", argv);
}

void zip(char* folder_name){
  char zip_name[50];
  char cwd[1024];
  char path[100];
  getcwd(cwd, sizeof(cwd));
  sprintf(zip_name, "%s.zip", folder_name);
  sprintf(path, "%s/%s/", cwd, folder_name); 
  char *argv[] = {"zip", "-r", zip_name, path, NULL}; 
  pid_t child_zip, child_rmdir;
  child_zip = fork();
  int status;
  if(child_zip == 0){
      execvp("zip", argv);
  }
  else{
    waitpid(child_zip, &status, 0);
    if(WIFEXITED(status)){
      child_rmdir = fork();
      if(child_rmdir == 0){
        removeDir(folder_name);
      }
    }
  }
}
```
Potongan kode diatas melakukan zip, caranya adalah dengan menambahkan command argumen pada argv dengan `zip -r time.zip, path` kemudian akan remove menggunakan waitpid(child_zip). Jadi akan menunggu zip tersebut sampai selesai kemudian melakukan remove directory. 

### 2.4 dan 2.5
```C
void terminateProgram(char *program_name, int mode, int pid){
  FILE *killer_file = fopen("killer", "w");
  if(mode == 1){
    fprintf(killer_file, "#!/bin/bash\npkill -f %s\nrm killer\n", program_name);
  }
  else {
    fprintf(killer_file, "#!/bin/bash\nkill %d\nrm killer\n", pid);
  }
  fclose(killer_file); 
  chmod("killer", 0700);
}
```
Terdapat 2 mode yaitu mode A dan mode B. Mode A yakni pada `fprintf(killer_file, "#!/bin/bash\npkill -f %s\nrm killer\n", program_name)`
command pkill -f program_name untuk menterminasi semua program yang bernama program_name 

Kemudian mode B yakni untuk menterminasi suatu pid yakni parent pid yang menjalankan while loop, sehingga while loop tersebut akan berhenti dan tidak akan membuat folder baru. Namun tetap membiarkan child process yang sedang berjalan menjalankan tugasnya hingga zip selesai.  

### 2.hasil
berikut merupakan isi dari folder setelah menjalankan lukisan.c :
![Screenshot_2023-03-24_at_22.24.53](/uploads/eadac389954d0fd4b66e8f3d7be87bf9/Screenshot_2023-03-24_at_22.24.53.png)
## nomor 3
### 3.hasil
## nomor 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

1. Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

2. Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

3. Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

4. Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
Bonus poin apabila CPU state minimum.

Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

### 4.1, 4.2, 4.3, 4.4, 4.5
```C
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>



int main(int argc, char *argv[]){
  char *cron_hours, *cron_minutes, *cron_seconds;

  cron_hours = argv[1];
  if (strcmp(cron_hours, "*") == 0) {
    cron_hours = "0";
  }

  cron_minutes = argv[2];
  if (strcmp(cron_minutes, "*") == 0) {
    cron_minutes = "0";
  }

  cron_seconds = argv[3];
  if (strcmp(cron_seconds, "*") == 0) {
    cron_seconds = "0";
  }

  int hours = atoi(cron_hours); 
  int minutes = atoi(cron_minutes); 
  int seconds = atoi(cron_seconds);

  if(hours > 23 || minutes > 59 || seconds > 59){
    printf("Invalid input value(s) for minutes, hours, or seconds\n");
    return 1;
  }

  if(access(argv[4], X_OK) == -1 ){
      fprintf(stderr, "File not found: %s\n", argv[4]);
      return 1;
    }

  time_t now = time(NULL);
  struct tm *tm_now = localtime(&now);

  struct tm tm_cron = *tm_now;
  tm_cron.tm_hour = hours;
  tm_cron.tm_min = minutes;
  tm_cron.tm_sec = seconds;

  pid_t pid = fork();

  if(pid == 0){
      while(1){

      time_t next_time = mktime(&tm_cron);

      if(next_time < now){
        next_time += 86400;
      }

      int sleep_time = (int)difftime(next_time,now);
      
      sleep(sleep_time);
      
      char *script_path = argv[4]; 
      char *args[] = {"sh", script_path, NULL};

      pid_t child_process = fork();

      if(child_process == 0){
        int result = execvp("sh", args);
        if(result == -1){
        perror("execvp");
        return EXIT_FAILURE; 
        }
      }
    }
  } 
  return 0;
}
```
cron_hours, cron_minutes, cron_seconds bertujuan untuk menyimpan argumen jam, menit, dan detik. Asterisk pada argumen memiliki default value 0. `time_t next_time = mktime(&tm_cron);` untuk membuat sebuah waktu dalam detik yang baru yang akan bertugas menjadi time selanjutnya.

Setelah itu membuat pesan error jika argumen tidak sesuai seperti format jam, menit, dan detik melebihi satuan internasional dan juga apabila file .sh tidak ditemukan.

Kemudian membuat fork untuk membuat child process yang menjalankan sleep program dan menunggu hingga waktu next_time. 
### 4.hasil
Hasil

Contoh program dijalankan:
![Screenshot_2023-03-24_at_23.10.24](/uploads/4e86744c136061cf9eb9e369dd20c4ad/Screenshot_2023-03-24_at_23.10.24.png)

Error salah format waktu:
![Screenshot_2023-03-24_at_23.14.11](/uploads/fd343bc9d2f7cfb65eccd24d3e5ba487/Screenshot_2023-03-24_at_23.14.11.png)
## kendala
1. cukup lama stuck dibagian cron karena tidak jalan-jalan tapi akhirnya bisa jalan juga
